
package com.unikum.web;

import com.unikum.data.Empire;
import com.unikum.data.EmpiresSQLRepository;
import com.unikum.data.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EditEmpireServlet", urlPatterns = {"/edit_empire"})
public class EditEmpireServlet extends HttpServlet {

    @EJB
    EmpireService empService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User sessUser = (User)request.getSession().getAttribute("user");
        String oldEmpName = request.getParameter("oldEmpireName");
        String empireName = request.getParameter("empireName");
        String empireDescription = request.getParameter("empireDescription");
        
        Empire empToEdit = empService.GetEmpireByName(sessUser, oldEmpName);
        try {
            empService.EditEmpire(empToEdit, empireName, empireDescription);
            getServletContext().getRequestDispatcher("/display_user_entities.jsp").include(request, response);
        } catch (Exception e) {
             response.getWriter().print("Could not edit empire.");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
