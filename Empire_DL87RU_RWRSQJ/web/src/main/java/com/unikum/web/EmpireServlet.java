package com.unikum.web;

import com.unikum.data.Empire;
import com.unikum.data.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EmpireServlet", urlPatterns = {"/new_empire"})
public class EmpireServlet extends HttpServlet {
    
    @EJB
    EmpireService empireService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
       User sessUser = (User)request.getSession().getAttribute("user");
       Empire emp = new Empire(request.getParameter("empire_name"),
                   request.getParameter("empire_description"), 
                   Long.parseLong(request.getParameter("empire_level")));
        try {
            emp.setUser(sessUser);
            empireService.createEmpire(emp);

            sessUser.getEmpires().add(emp);
            
           request.setAttribute("user", sessUser);
           getServletContext().getRequestDispatcher("/display_user_entities.jsp").include(request, response);
        
        } catch (Exception e) {
           response.getWriter().print("Cannot create! message: "+ e.getMessage());
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
