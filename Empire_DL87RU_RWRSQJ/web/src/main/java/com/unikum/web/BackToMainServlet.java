package com.unikum.web;

import com.unikum.data.SpeciesSQLRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "BackToMainServlet", urlPatterns = {"/backToMain"})
public class BackToMainServlet extends HttpServlet {

    @Inject
    SpeciesSQLRepository speciesRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("species", speciesRepository.getSpecies());
        getServletContext().getRequestDispatcher("/hero.jsp").include(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
