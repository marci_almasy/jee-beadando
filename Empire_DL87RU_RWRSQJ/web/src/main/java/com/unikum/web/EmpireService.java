package com.unikum.web;

import com.unikum.data.Building;
import com.unikum.data.BuildingSQLRepository;
import com.unikum.data.Empire;
import com.unikum.data.EmpiresSQLRepository;
import com.unikum.data.HeroSQLRepository;
import com.unikum.data.NaturalAsset;
import com.unikum.data.NaturalAssetSQLRepository;
import com.unikum.data.People;
import com.unikum.data.PeopleSQLRepository;
import com.unikum.data.Population;
import com.unikum.data.Stock;
import com.unikum.data.User;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class EmpireService {
    
    Empire empToEdit;

    
    private Random rnd = new Random(); 
    
    @Inject
    EmpiresSQLRepository empireRepo;
    
     @Inject
     NaturalAssetSQLRepository naturalAssetrepo;
     
     @Inject
     PeopleSQLRepository peopleRepo;
     
     
     @Inject
     BuildingSQLRepository buildingRepo;
     
     
    public Empire getEmpToEdit() {
        return empToEdit;
    }

    public void setEmpToEdit(Empire empToEdit) {
        this.empToEdit = empToEdit;
    }
        
     public Empire createEmpire(Empire empire){
        List<Empire> empires = empireRepo.GetEmpiresByUser(empire.getEmpireName(), empire.getUser());
        if (empires.size()>0) {
            throw new EmpireCreationFailedException();
        }
        
         for (int i = 0; i < rnd.nextInt(3)+2; i++) {
            ProduceAndWarehauseCreate(empire);
        }
         
        for (int i = 0; i < rnd.nextInt(3)+2; i++) {
            PopulationCreate(empire);
        }
         
         for (int i = 0; i < rnd.nextInt(3)+2; i++) {
            BuildingCreate(empire);
        }
        empireRepo.addEmpire(empire);
        return empire;
    }
     void ProduceAndWarehauseCreate(Empire empire){
            NaturalAsset randomAsset =  naturalAssetrepo.getnaturalAssets().get(rnd.nextInt(naturalAssetrepo.getnaturalAssets().size()));
            Stock st = new Stock(randomAsset,rnd.nextInt(200)+2);
            st.setEmpire(empire);
            empire.getProduce().add(st);
            st = new Stock(randomAsset,rnd.nextInt(500)+2);
            st.setEmpire(empire);
            empire.getWareHouse().add(st);
     }
     
     void PopulationCreate(Empire empire){
         People randomPeople = peopleRepo.getPeople().get(rnd.nextInt(peopleRepo.getPeople().size()));
         Population pop = new Population(randomPeople,rnd.nextInt(2)+2);
         empire.getPopulation().add(pop);
     }
     
     void BuildingCreate(Empire empire){
         Building randomBuilding  = buildingRepo.getBuildings().get(rnd.nextInt(buildingRepo.getBuildings().size()));
         randomBuilding.setEmpire(empire);
         empire.getBuildings().add(randomBuilding);
     }
     
     public void DeleteEmpire(User user, String empName){
         Empire emp = GetEmpireByName(user, empName);
         empireRepo.deleteEmpire(emp);
     }
     
     public void EditEmpire(Empire emp, String newName, String newDescription){
         empireRepo.editEmpire(emp, newName, newDescription);
     }
     
     public Empire GetEmpireByName(User user, String empName){
         return empireRepo.GetEmpireByUser(empName, user);
     }
}
