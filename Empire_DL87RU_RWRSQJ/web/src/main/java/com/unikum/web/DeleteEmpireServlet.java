
package com.unikum.web;

import com.unikum.data.EmpiresSQLRepository;
import com.unikum.data.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "DeleteEmpireServlet", urlPatterns = {"/delete_empire"})
public class DeleteEmpireServlet extends HttpServlet {
    
    @EJB
    EmpireService empireService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            empireService.DeleteEmpire((User)request.getSession().getAttribute("user"), request.getParameter("empireName"));
            getServletContext().getRequestDispatcher("/display_user_entities.jsp").include(request, response);

        } catch (Exception e) {
            response.getWriter().print("Could not delete empire.");
        }
    }
     

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
