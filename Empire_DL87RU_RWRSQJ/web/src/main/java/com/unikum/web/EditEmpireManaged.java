package com.unikum.web;

import com.unikum.data.Empire;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean(name = "editEmpireManaged", eager = true)
public class EditEmpireManaged {
    
    @Inject
    UserService userService;
    
    @Inject
    EmpireService empService;
    
    String oldEmpireName;
    
    String  empToEditName;    
    String  empToEditDescription;

    public String getOldEmpireName() {
        return empService.getEmpToEdit().getEmpireName();
    }

    public void setOldEmpireName(String oldEmpireName) {
        this.oldEmpireName = oldEmpireName;
    }

    public String getEmpToEditName() {
        oldEmpireName= empService.getEmpToEdit().getEmpireName();
        return empService.getEmpToEdit().getEmpireName();
    }

    public void setEmpToEditName(String empToEditName) {
        this.empToEditName = empToEditName;
    }

    public String getEmpToEditDescription() {
        return empService.getEmpToEdit().getEmpireDescription();
    }

    public void setEmpToEditDescription(String empToEditDescription) {
        this.empToEditDescription = empToEditDescription;
    }

    public void Edit() throws IOException{
        Empire empToEdit = empService.GetEmpireByName(userService.getSessUser(), oldEmpireName);
        empService.EditEmpire(empToEdit, empToEditName, empToEditDescription);
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/display_user_entities.xhtml");
    }
}
