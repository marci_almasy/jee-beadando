package com.unikum.web;

import com.unikum.data.LoginFailedException;
import com.unikum.data.NaturalAssetSQLRepository;
import com.unikum.data.SpeciesSQLRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
    //UserRepository users = new  UserRepository();
    @Inject
    UserService userService;
    
    @Inject
    SpeciesSQLRepository speciesRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");        
        String password =request.getParameter("password");
        try {
            request.getSession().setAttribute("user", userService.Login(name, password));
            request.setAttribute("species", speciesRepository.getSpecies());
            getServletContext().getRequestDispatcher("/hero.jsp").include(request, response);
        } catch (LoginFailedException e) {
            response.getWriter().print("Could not log in.");
         }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
