package com.unikum.web;

import com.unikum.data.Empire;
import com.unikum.data.EmpiresSQLRepository;
import com.unikum.data.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "UserEntitiesServlet", urlPatterns = {"/user_entities"})
public class UserEntitiesServlet extends HttpServlet {

    @Inject
    EmpiresSQLRepository empiresRepo;
     
    @Override 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User sessUser = (User)request.getSession().getAttribute("user");

        List<Empire> existingEmpires = sessUser.getEmpires();
        List<String> empireNames = new ArrayList<>();
         for (Empire emp : existingEmpires) {
            empireNames.add(emp.getEmpireName());
        }
         
         /*
         for (Empire emp : empiresRepo.getEmpires()) {
            if (!empireNames.contains(emp.getEmpireName())) {
                sessUser.getEmpires().add(emp);
                empireNames.add(emp.getEmpireName());
            }
        }*/
        getServletContext().getRequestDispatcher("/display_user_entities.jsp").include(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
