package com.unikum.web;

import com.unikum.data.LoginFailedException;
import com.unikum.data.RegistrationNotHappenedException;
import com.unikum.data.User;
import com.unikum.data.UserSQLRepository;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserService {
    @Inject
     UserSQLRepository userRepo;
    
    User sessUser;

    public User getSessUser() {
        return sessUser;
    }

    public void setSessUser(User sessUser) {
        this.sessUser = sessUser;
    }
    
    public void Registration(String name, String password) throws RegistrationNotHappenedException{
        try {
            //TODO: végigforozni   az egészen nevek alapján
            userRepo.GetByName(name); //dobhat exceptiont
            userRepo.addUser(new User(name, password, false));
        } catch (Exception e) {
            throw new RegistrationNotHappenedException();
        }
        
        /*
        User tmpUser = new User(name, password, false);

        try {
            userRepo.GetByName(tmpUser.getName());
            userRepo.addUser(tmpUser);
        } catch (Exception e) {
            throw new RegistrationNotHappenedException();
        }
        
        for (User u : userRepo.getUsers()) {
            if (u.getName().equals(name)) {
                throw new RegistrationNotHappenedException();
            }
        }*/
        
        /*
        em.getTransaction().begin();
        for (Hero h : tmpUser.getHeroes()) {
            em.persist(h);
        }
        
        for (Empire e : tmpUser.getEmpires()) {
            em.persist(e);
        }
        em.persist(tmpUser);
        em.getTransaction().commit();
        */
    }
        
    public User Login(String name, String password) throws LoginFailedException{
       /* for (User u : userRepo.getUsers()) {
            if (u.getName().equals(name)&& u.getPassword().equals(password)) {
                return u;
            }
        }
        throw new LoginFailedException();*/
       
       try {
              return userRepo.GetByNamePassword(name, password); //dobhat exceptiont

        } catch (Exception e) {
            throw new LoginFailedException();
        }
    }
}
