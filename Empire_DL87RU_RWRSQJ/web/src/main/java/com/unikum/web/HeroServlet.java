package com.unikum.web;

import com.unikum.data.Hero;
import com.unikum.data.Hybrid;
import com.unikum.data.Species;
import com.unikum.data.SpeciesSQLRepository;
import com.unikum.data.User;
import java.io.IOException;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "HeroServlet", urlPatterns = {"/newhero"})
public class HeroServlet extends HttpServlet {
    
    @Inject
    SpeciesSQLRepository speciesRepository;
    
    @EJB
    HeroService heroService;
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Hero hero = new Hero(request.getParameter("hero_name"),request.getParameter("hero_description"));
       
        for(Species spc:speciesRepository.getSpecies()){
           Hybrid newHybrid = new Hybrid(spc,Byte.parseByte(request.getParameter(spc.getSpeciesName())));
           hero.getHybrids().add(newHybrid);
       }
        
       
       User sessUser = (User)request.getSession().getAttribute("user");
       hero.setUser(sessUser);
       heroService.add(hero);

       sessUser.getHeroes().add(hero);
       getServletContext().getRequestDispatcher("/display_user_entities.jsp").include(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
