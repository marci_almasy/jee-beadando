package com.unikum.web;

import com.unikum.data.Empire;
import java.io.IOException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean(name = "userEntitiesManaged", eager = true)
public class UserEntitiesManaged {
    @Inject
    UserService userService;
    
    @Inject
    EmpireService empService;
    
    String empNameToDelete;

    public String getEmpNameToDelete() {
        return empNameToDelete;
    }

    public void setEmpNameToDelete(String empNameToDelete) {
        this.empNameToDelete = empNameToDelete;
    }
    
    public List<Empire> UserEmpires() {
        return userService.sessUser.getEmpires();
    }
    
    
    public void BackToMain() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/hero.xhtml");
    }
    
     public void EditEmpire(String empNameToEdit) throws IOException{
         empService.setEmpToEdit(empService.GetEmpireByName(userService.getSessUser(), empNameToEdit));
         FacesContext.getCurrentInstance().getExternalContext().dispatch("/edit_empire.xhtml");

    }
    
    public void DeleteEmpire(String empToDel) throws IOException{
        empService.DeleteEmpire(userService.getSessUser(), empToDel);
        //FacesContext.getCurrentInstance().getExternalContext().dispatch("/display_user_entities.xhtml");
    }
    
}
