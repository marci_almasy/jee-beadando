package com.unikum.web;

import com.unikum.data.Empire;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean(name = "heroManaged", eager = true)
public class HeroManaged {
    @Inject
    EmpireService empService;
    
    @Inject
    UserService userService;
    
    
    Empire newEmpire;
    
    String empName;
    String empDescription;  
    int empLevel;


    public Empire getNewEmpire() {
        return newEmpire;
    }

    public void setNewEmpire(Empire newEmpire) {
        this.newEmpire = newEmpire;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpDescription() {
        return empDescription;
    }

    public void setEmpDescription(String empDescription) {
        this.empDescription = empDescription;
    }

    public int getEmpLevel() {
        return empLevel;
    }

    public void setEmpLevel(int empLevel) {
        this.empLevel = empLevel;
    }
    
    public void CreateEmpire() throws IOException{
        newEmpire = new Empire(empName,empDescription,empLevel);
        newEmpire.setUser(userService.getSessUser());
        empService.createEmpire(newEmpire);
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/display_user_entities.xhtml");
    }
    
    public void DisplayUserEntities() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/display_user_entities.xhtml");
    }
    
   public void LogOut() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/home.xhtml");
    }
     
}
