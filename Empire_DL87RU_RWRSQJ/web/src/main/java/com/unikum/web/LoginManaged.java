package com.unikum.web;

import com.unikum.data.LoginFailedException;
import com.unikum.data.RegistrationNotHappenedException;
import com.unikum.data.SpeciesSQLRepository;
import com.unikum.data.User;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean(name = "loginManaged", eager = true)
public class LoginManaged {
    @Inject
    UserService userService;
    
    @Inject
    SpeciesSQLRepository speciesRepository;
    
    User sessUser;
    
    String userName;
    String userPassword;    
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public SpeciesSQLRepository getSpeciesRepository() {
        return speciesRepository;
    }

    public void setSpeciesRepository(SpeciesSQLRepository speciesRepository) {
        this.speciesRepository = speciesRepository;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
   
    public void Login() throws LoginFailedException, IOException{
        sessUser = userService.Login(userName,userPassword);
        userService.setSessUser(sessUser);
        FacesContext.getCurrentInstance().getExternalContext().dispatch("/hero.xhtml");
    }
    
    public void Register() throws IOException, RegistrationNotHappenedException{
        userService.Registration(userName, userPassword);
        status = "Registered!";
    }
}
