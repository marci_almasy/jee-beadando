package com.unikum.web;

import com.unikum.data.Hero;
import com.unikum.data.HeroSQLRepository;
import com.unikum.data.Hybrid;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class HeroService {
    
    @Inject
    HeroSQLRepository heroRepository;
    
    public Hero add(Hero hero){
        byte sum = 0;
        if (hero != null && hero.getHybrids()!= null) {
             for (Hybrid h : hero.getHybrids()) {
                sum += h.getPercentage();
             }
             if (sum!= 100) {
                throw new ValidateHeroRuntimeException();
            }
        }
       
        List<Hero> heroes = heroRepository.GetHeroesByUser(hero.getHeroName(), hero.getUser());
        if (heroes.size()>0) {
            throw new ValidateHeroRuntimeException();
        }
        heroRepository.addHero(hero);
        return hero;
    }
    
    //ehelyett heroRepository.getHeroes()
    public List<Hero> getHeroes(){
        return heroRepository.getHeroes();
    }
}
