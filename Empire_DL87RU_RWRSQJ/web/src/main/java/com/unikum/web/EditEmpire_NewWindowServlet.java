
package com.unikum.web;

import com.unikum.data.Empire;
import com.unikum.data.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EditEmpire_NewWindowServlet", urlPatterns = {"/edit_empire_newWindow"})
public class EditEmpire_NewWindowServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User sessUser = (User)request.getSession().getAttribute("user");
        String empireName  = (String)request.getParameter("empireName");
        //String empireDescription  = (String)request.getParameter("empireDescription");
        
        Empire  empireToEdit = sessUser.getEmpireByName(empireName);
        
        request.setAttribute("empireToEdit", empireToEdit);
        
        
        
        getServletContext().getRequestDispatcher("/edit_empire_information.jsp").include(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
