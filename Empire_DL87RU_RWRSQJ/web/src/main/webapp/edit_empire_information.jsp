
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Empire Information</title>
    </head>
    <body>
        <h1>Edit Empire Information</h1>
        <form method="post" action="edit_empire">
            <input type="hidden" name="oldEmpireName"  value="${empireToEdit.empireName}">
            Edit empire name: <input type="text" name="empireName" value="${empireToEdit.empireName}"><br>
            Edit empire description: <input type="text" name="empireDescription" value="${empireToEdit.empireDescription}"><br>
            <input type="submit" value="Edit">
        </form>
    </body>
</html>
