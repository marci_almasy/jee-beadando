  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form method="post" action="newhero">
    <h1> New Hero:</h1>
    Hero name: <input type="text" name="hero_name"> <br>
    Description: <input type="type" name="hero_description"> <br>
    <c:forEach var="spc" items="${species}">
       ${spc.speciesName}<input type="text" name="${spc.speciesName}">
    </c:forEach><br>
    <input type="submit" value="Add Hero"> 

    
</form>
<form  method="post" action="new_empire">
        <h1> New Empire:</h1>
    Empire name: <input type="text" name="empire_name"> <br>
    Empire description: <input type="type" name="empire_description"> <br>
    Empire Level: <input type="type" name="empire_level"> <br>
    <input type="submit" value="Add Empire"> 
</form>

<form  method="post" action="user_entities">
    <input type="submit" value="User entities"> 
</form>

<form  method="post" action="/web">
    <input type="submit" value="Log out"> 
</form>