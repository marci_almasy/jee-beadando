  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Entities</title>
    </head>
    <body>
        <form  method="post" action="backToMain">
                <input type="submit" value="Back"> 
        </form>
        <table border=1>
            <tr>
                <th><h1>Heroes</h1></th>
            </tr>
                <c:forEach var="hero" items="${user.getHeroes()}">
                    <tr>
                        <td>
                            <b>Hero name: </b>${hero.heroName} <br/>
                        </td>
                    </tr> 
                </c:forEach>
        </table>
        <table border=1>
            <tr>
                <th><h1>Empires</h1></th>
            </tr>
                <c:forEach var="empire" items="${user.getEmpires()}">
                    <tr>
                        <td>
                            <b>Empire name: </b>${empire.empireName} <br/>                            
                            <b>Empire description: </b><br/>${empire.empireDescription} <br/> 
                            
                            <form method="post" action="edit_empire_newWindow">
                                <input type="hidden" name="empireName" value="${empire.empireName}"><br>
                                <input type="hidden" name="empireDescription" value="${empire.empireDescription}"><br>
                                <input type="submit" value="Edit">
                            </form>
                             
                            <b>Empire level: </b>${empire.level} <br/>
                            <b>Population: </b><br/>
                            <ul>
                                <c:forEach var="pop" items="${empire.population}">
                                        <li>${pop.people.name} Quantity: ${pop.quantity} </li> 
                                </c:forEach>
                            </ul>
                            <b>Stock produce: </b> <br/>
                            <ul>
                                <c:forEach var="stock" items="${empire.produce}">
                                        <li>${stock.asset.description} ${stock.asset.name} Quantity: ${stock.quantity}/h </li> 
                                </c:forEach>
                            </ul>
                            <b>Stock warehouse: </b> <br/>
                            <ul>
                                <c:forEach var="wh" items="${empire.wareHouse}">
                                        <li>${wh.asset.description} ${wh.asset.name} Amount: ${wh.quantity}</li>
                                </c:forEach>
                            </ul>
                            <b>Buildings: </b> <br/>
                            <ul>
                                <c:forEach var="b" items="${empire.buildings}">
                                        <li>${b.buildingName} (
                                            <c:forEach var="c" items="${b.cost}">
                                                ${c.asset.name} - ${c.quantity},
                                            </c:forEach>
                                            )</li>
                                </c:forEach>
                            </ul>
                            <form method="post" action="delete_empire">
                                <input type="hidden" name="empireName" value="${empire.empireName}"> 
                                 <input type="submit" value="Delete"> 
                            </form> <br> 
                            
                        </td>
                    </tr> 
                </c:forEach>
        </table>
    </body>
</html>
