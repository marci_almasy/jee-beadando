
package com.unikum.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class ApplicationConfiguration {
    
    private Random rnd = new Random(); 
    
    @Produces
    @ApplicationScoped
    public UserSQLRepository createUserRepository()
    {
        UserSQLRepository ur = new UserSQLRepository();
        try
        {
            if (ur.getUsers().isEmpty()) {
                ur.addUser(new User("a", "a", false)); // a tanárnál ez addUser
                ur.addUser(new User("b", "b", false));
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return ur;
    }
    
    @ApplicationScoped
    @Produces
    public SpeciesSQLRepository createSpeciesRepository()
    {
        SpeciesSQLRepository sr = new SpeciesSQLRepository();
        try
        {
            if (sr.getSpecies().isEmpty()) {
                sr.addSpecie(new Species("Ember", "okos"));
                sr.addSpecie(new Species("Törpe", "buta"));
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return sr;
    }
    
    @ApplicationScoped
    NaturalAssetSQLRepository nr;
    
    @ApplicationScoped
    @Produces
    public NaturalAssetSQLRepository createNaturalAssetRepository()
    {
        nr = new NaturalAssetSQLRepository();
        List<String> names = Arrays.asList("vas", "rez", "acel", "arany", "komuvesaktimel", "rizsa", "flex", "palinka", "72-es tatra", "parizer");
        List<String> descriptions = Arrays.asList("kemeny", "kekeres", "utos", "hangos", "f@sza", "hatasos", "megborítos", "herbalos", "gyilkos");
        
        try
        {
            if (nr.getnaturalAssets().isEmpty()) {
                for (int i = 0; i < rnd.nextInt(7)+2; i++) {
                    int rndNamesIdx = rnd.nextInt(names.size());
                    int rndDescriptionsIdx = rnd.nextInt(descriptions.size());
                    nr.addNaturalAsset(new NaturalAsset(names.get(rndNamesIdx), descriptions.get(rndDescriptionsIdx)));
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return nr;
    }
    
    @ApplicationScoped
    @Produces
    public PeopleSQLRepository createPeopleRepository()
    {
        PeopleSQLRepository sr = new PeopleSQLRepository();
        
        List<String> names = Arrays.asList("Gyuri", "Lacci","Jezus", "Guccsi", "Rikardo", "Brenddon");
        List<String> descriptions = Arrays.asList("veszelyes", "ovatos","meszaros", "penztaros", "vastudos", "vasmajas", "miniszterelnokos", "rajos", "dzsigolos");
        
        try
        {
            if (sr.getPeople().isEmpty()) {
                for (int i = 0; i < rnd.nextInt(11)+2; i++) {
                    int rndNamesIdx = rnd.nextInt(names.size());
                    int rndDescriptionsIdx = rnd.nextInt(descriptions.size());
                    sr.addPeople(new People(names.get(rndNamesIdx), descriptions.get(rndDescriptionsIdx)));
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return sr;
    }
    
    
    @ApplicationScoped
    @Produces
    public BuildingSQLRepository createBuildingRepository()
    {
        BuildingSQLRepository br = new BuildingSQLRepository();
               
        List<String> names = Arrays.asList("Sorgyar", "Templom", "Banya", "Froccsbar", "SPAR", "Becsuletsullyeszto", "Blaha  aluljáro", "Parlament", "Soros kiláto", "Parhuzamos kilato");
        List<String> descriptions = Arrays.asList("ubekiraly", "fulladak", "gizdda", "veszelyes", "biztonsagos", "wunderschon", "fos");
       
        
        
        try
        {
            if (br.getBuildings().isEmpty()) {
                for (int j = 0; j < 5; j++) {
                    List<Stock> costs =new ArrayList<>();
                    List<String> costsNames =new ArrayList<>();
                    for (int i = 0; i < nr.getnaturalAssets().size()+5; i++) {
                    Stock buildCost  = new Stock(nr.getnaturalAssets().get(rnd.nextInt(nr.getnaturalAssets().size())),rnd.nextInt(500)+200);
                    if (!costsNames.contains(buildCost.getAsset().getName())) {
                        costsNames.add(buildCost.getAsset().getName());
                        costs.add(buildCost);
                        }
                     }
                  Building returnBuilding= new Building(names.get(rnd.nextInt(names.size())), descriptions.get(rnd.nextInt(descriptions.size())), rnd.nextInt(5)+1);
                  returnBuilding.setCost(costs);
                  br.addBuilding(returnBuilding);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return br;
    }
    
    @ApplicationScoped
    @Produces
    public HeroSQLRepository createHeroRepository()
    {
        HeroSQLRepository hr = new HeroSQLRepository();
        
        return hr;
    }
    
    @ApplicationScoped
    @Produces
    public EmpiresSQLRepository createEmpireRepository()
    {
        EmpiresSQLRepository er = new EmpiresSQLRepository();
        
        return er;
    }
    
}
