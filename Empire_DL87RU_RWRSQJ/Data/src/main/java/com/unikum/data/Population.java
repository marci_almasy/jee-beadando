
package com.unikum.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "population")
public class Population {
    @ManyToOne
    private People people;
    
    private long quantity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    private Empire empire;
    
    //@ManyToOne
   // private Empire empire;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Population(People people, long quantity) {
        this.people = people;
        this.quantity = quantity;
    }
    
    public Population()
    {
        
    }
}
