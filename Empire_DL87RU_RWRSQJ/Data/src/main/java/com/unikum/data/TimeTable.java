
package com.unikum.data;
import java.util.Date;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "time_table")
public class TimeTable {
    @OneToOne
    private Hero hero;
    
    @OneToOne
    private Empire empire;
    
    private Timestamp startTime;
    private Timestamp stopTime;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
  

    public TimeTable(Hero hero, Empire empire, Timestamp startTime, Timestamp stopTime) {
        this.hero = hero;
        this.empire = empire;
        this.startTime = startTime;
        this.stopTime = stopTime;
    }
    
    public TimeTable()
    {
        
    }

    public long getId() {
        return id;
    }


    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Empire getEmpire() {
        return empire;
    }

    public void setEmpire(Empire empire) {
        this.empire = empire;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getStopTime() {
        return stopTime;
    }

    public void setStopTime(Timestamp stopTime) {
        this.stopTime = stopTime;
    }
    
    
}
