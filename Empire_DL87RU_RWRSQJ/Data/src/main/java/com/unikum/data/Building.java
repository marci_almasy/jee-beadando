package com.unikum.data;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "building")
public class Building {
    private String buildingName;
    private String buildingDescription;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @OneToMany
    private List<Stock> cost = new ArrayList<>();
    
    private long buildTime;
    @ManyToOne
    private Empire empire;
    
    public long getId() {
        return id;
    }

    public Empire getEmpire() {
        return empire;
    }

    public void setEmpire(Empire empire) {
        this.empire = empire;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingDescription() {
        return buildingDescription;
    }

    public void setBuildingDescription(String buildingDescription) {
        this.buildingDescription = buildingDescription;
    }

    public List<Stock> getCost() {
        return cost;
    }

    public void setCost(List<Stock> cost) {
        this.cost = cost;
    }

    public long getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(long buildTime) {
        this.buildTime = buildTime;
    }

    public Building(String buildingName, String buildingDescription, long buildTime) {
        this.buildingName = buildingName;
        this.buildingDescription = buildingDescription;
        this.buildTime = buildTime;
    }
    
    public Building()
    {
        
    }
}
