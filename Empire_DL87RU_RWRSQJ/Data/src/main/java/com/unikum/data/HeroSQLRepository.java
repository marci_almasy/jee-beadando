package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class HeroSQLRepository {
    

   private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public List<Hero> getHeroes() {
        return em.createQuery("SELECT h FROM Hero h", Hero.class).getResultList();
    }
    
    public void addHero(Hero pValue)
    {
        em.getTransaction().begin();
        //OneToMany miatt kell
        
        for (Hybrid h : pValue.getHybrids()) {
            em.persist(h);
        }
        em.persist(pValue);
        em.getTransaction().commit();
    }
    
    public List<Hero> GetHeroesByUser(String heroName, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Hero.class);
        Root root = cq.from(Hero.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"), user),cb.equal(root.get("heroName"), heroName)));            
        return em.createQuery(cq).getResultList();
    }
}
