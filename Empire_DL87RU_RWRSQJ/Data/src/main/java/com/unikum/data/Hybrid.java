package com.unikum.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hybrid")
public class Hybrid {
    @ManyToOne
    private Species species;
    
    private byte percentage;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
     public byte getPercentage() {
        return percentage;
    }

    public void setPercentage(byte percentage) {
        this.percentage = percentage;
    }
    

    public Species getSpecies() {
        return species;
    }

    public Hybrid(Species species, byte percentage) {
        this.species = species;
        this.percentage = percentage;
    }

    public Hybrid() {
    }
    
    
}
