package com.unikum.data;

import java.util.ArrayList;
import java.util.List;

public class EmpiresObjectRepository {
    private List<Empire> empires = new ArrayList<>();

    public List<Empire> getEmpires() {
        return empires;
    }

    public EmpiresObjectRepository() {
        
    }
    
    public boolean createEmpire(User user, String name, String empireName, long level)
    {
        Empire e = new Empire(name, empireName, level);
                
        List<String> empireNames = new ArrayList<>();
        for (Empire emp : empires) {
            empireNames.add(emp.getEmpireName());
        }
        
        if(!empireNames.contains(e.getEmpireName())){
            user.getEmpires().add(e);
            empires.add(e);
            return true;
        }
        
        return false;
    }
    
    public void deleteEmpire(User user, String empireName){
        Empire empToDelete = user.getEmpireByName(empireName);
        user.getEmpires().remove(empToDelete);
        empires.remove(empToDelete);
    }
    
    public boolean editEmpire(User user, String empToEditName,String newEmpireName, String newEmpireDescription){
         Empire empToEdit = user.getEmpireByName(empToEditName);
         List<String> empiresNames = new ArrayList<>();
         for (Empire emp : empires) {
            empiresNames.add(emp.getEmpireName());
        }
         if (!empiresNames.contains(newEmpireName)) {
            empToEdit.setEmpireName(newEmpireName);
            return true;
        }
        empToEdit.setEmpireDescription(newEmpireDescription);
        return false;
    }
    
}
