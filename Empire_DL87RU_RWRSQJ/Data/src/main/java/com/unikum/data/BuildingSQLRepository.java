package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class BuildingSQLRepository {
     private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public List<Building> getBuildings() {
        return em.createQuery("SELECT b FROM Building b", Building.class).getResultList();
    }
    
    public void addBuilding(Building pValue)
    {
        em.getTransaction().begin();
        for (Stock s : pValue.getCost()) {
                //em.persist(s.getAsset());
                em.persist(s);
            }
        em.persist(pValue);
        em.getTransaction().commit();
    }
    
}
