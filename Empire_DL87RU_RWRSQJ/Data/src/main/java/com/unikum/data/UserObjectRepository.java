package com.unikum.data;

import java.util.ArrayList;
import java.util.List;

public class UserObjectRepository {
    
    private List<User> users = new ArrayList<>();

    public UserObjectRepository() {
    }
    
    
    
    public void Registration(String name, String password) throws RegistrationNotHappenedException{
        for (User u : users) {
            if (u.getName().equals(name)) {
                throw new RegistrationNotHappenedException();
            }
        }
        
        User tmpUser = new User(name, password, false);
        users.add(tmpUser);
    }
        
    public User Login(String name, String password) throws LoginFailedException{
        for (User u : users) {
            if (u.getName().equals(name)&& u.getPassword().equals(password)) {
                return u;
            }
        }
        throw new LoginFailedException();
    }
}
