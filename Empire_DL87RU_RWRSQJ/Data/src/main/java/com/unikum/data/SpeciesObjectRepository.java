package com.unikum.data;

import java.util.ArrayList;
import java.util.List;

public class SpeciesObjectRepository {
    
    private List<Species> species = new ArrayList<>();

    public List<Species> getSpecies() {
        return species;
    }
    
    public void addSpecie(Species pValue)
    {
       this.species.add(pValue); 
    }
    
}
