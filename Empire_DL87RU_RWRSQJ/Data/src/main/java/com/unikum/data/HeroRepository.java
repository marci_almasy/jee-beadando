package com.unikum.data;

import java.util.ArrayList;
import java.util.List;

public class HeroRepository {
    

    private List<Hero> heroes = new ArrayList<>();

    public List<Hero> getHeroes() {
        return heroes;
    }
    
    public void addHero(Hero pValue)
    {
       this.heroes.add(pValue); 
    }
    
}
