
package com.unikum.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ability")
public class Ability {
    private byte force;
    private byte brain;
    private byte skill;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    public byte getForce() {
        return force;
    }

    public void setForce(byte force) {
        this.force = force;
    }

    public byte getBrain() {
        return brain;
    }

    public void setBrain(byte brain) {
        this.brain = brain;
    }

    public byte getSkill() {
        return skill;
    }

    public void setSkill(byte skill) {
        this.skill = skill;
    }

    public Ability(byte force, byte brain, byte skill) {
        this.force = force;
        this.brain = brain;
        this.skill = skill;
    }
    
    
    public Ability()
    {
        
    }
}
