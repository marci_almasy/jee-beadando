
package com.unikum.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "empire")
public class Empire {
    private String empireName;
    private String empireDescription;
    private long level;
   
    @OneToMany(mappedBy = "empire", cascade = CascadeType.REMOVE)
    private List<Population> population = new ArrayList<>();
   
    
    @OneToMany(mappedBy = "empire", cascade = CascadeType.REMOVE)
    private List<Stock> produce = new ArrayList<>();
   
    
    @OneToMany(mappedBy = "empire",cascade = CascadeType.REMOVE)
    private List<Stock> wareHouse = new ArrayList<>();
    
    @OneToMany(mappedBy = "empire",cascade = CascadeType.REMOVE)
    private List<Building> buildings = new ArrayList<>();
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private User user;
    
     public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Random getRnd() {
        return rnd;
    }

    public void setRnd(Random rnd) {
        this.rnd = rnd;
    }

    public long getId() {
        return id;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }
    private Random rnd = new Random(); 

    public String getEmpireName() {
        return empireName;
    }

    public void setEmpireName(String empireName) {
        this.empireName = empireName;
    }

    public String getEmpireDescription() {
        return empireDescription;
    }

    public void setEmpireDescription(String empireDescription) {
        this.empireDescription = empireDescription;
    }

    

    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    public List<Population> getPopulation() {
        return population;
    }

    public void setPopulation(List<Population> population) {
        this.population = population;
    }

    public List<Stock> getProduce() {
        return produce;
    }

    public void setProduce(List<Stock> produce) {
        this.produce = produce;
    }

    public List<Stock> getWareHouse() {
        return wareHouse;
    }

    public void setWareHouse(List<Stock> wareHouse) {
        this.wareHouse = wareHouse;
    }

    public Empire(String empireName, String empireDescription, long level) {
        this.empireName = empireName;
        this.empireDescription = empireDescription;
        this.level = level;
        
        /*
        for (int i = 0; i < rnd.nextInt(3)+2; i++) {
            this.population.add(FillRandomPop());
        }
        
        for (int i = 0; i < rnd.nextInt(3)+2; i++) {
            Stock st = FillRandomStock();
            this.produce.add(st);
            this.wareHouse.add(new Stock(new NaturalAsset(st.getAsset().getName(), st.getAsset().getDescription()), rnd.nextInt(500) + 100));
        }
        for (Stock st : wareHouse) {
            st.setQuantity(rnd.nextInt(500) + 100);
        }*/
        /*
        for (int i = 0; i < rnd.nextInt(2); i++) {
            this.buildings.add(FillRandomBuilding());
        }*/
    }
    
    public Empire()
    {
        
    }
   
    
    Population FillRandomPop(){
        List<String> names = Arrays.asList("Gyuri", "Lacci","Jezus", "Guccsi", "Rikardo", "Brenddon");
        List<String> descriptions = Arrays.asList("veszelyes", "ovatos","meszaros", "penztaros", "vastudos", "vasmajas", "miniszterelnokos", "rajos", "dzsigolos");
        
        int rndNamesIdx = rnd.nextInt(names.size());
        int rndDescriptionsIdx = rnd.nextInt(descriptions.size());
        return new Population(new People(names.get(rndNamesIdx), descriptions.get(rndDescriptionsIdx)),rnd.nextInt(10) + 2);
    }
    /*
     Stock FillRandomStock(){
        List<String> names = Arrays.asList("vas", "rez", "acel", "arany", "komuvesaktimel", "rizsa", "flex", "palinka", "72-es tatra", "parizer");
        List<String> descriptions = Arrays.asList("kemeny", "kekeres", "utos", "hangos", "f@sza", "hatasos", "megborítos", "herbalos", "gyilkos");
        
        int rndNamesIdx = rnd.nextInt(names.size());
        int rndDescriptionsIdx = rnd.nextInt(descriptions.size());
        return new Stock(new NaturalAsset(names.get(rndNamesIdx), descriptions.get(rndDescriptionsIdx)), rnd.nextInt(10) + 2);
    }*/
     
      Building FillRandomBuilding(){
        List<String> names = Arrays.asList("Sorgyar", "Templom", "Banya", "Froccsbar", "SPAR", "Becsuletsullyeszto", "Blaha  aluljáro", "Parlament", "Soros kiláto", "Parhuzamos kilato");
        List<String> descriptions = Arrays.asList("ubekiraly", "fulladak", "gizdda", "veszelyes", "biztonsagos", "wunderschon", "fos");
        List<Stock> costs =new ArrayList<>();
        List<String> costsNames =new ArrayList<>();
        
          for (int i = 0; i < 4; i++) {
              
               Stock buildCost  = wareHouse.get(rnd.nextInt(wareHouse.size()));
               if (!costsNames.contains(buildCost.getAsset().getName())) {
                   costsNames.add(buildCost.getAsset().getName());
                   Stock newBuildCost = new Stock(new NaturalAsset(buildCost.getAsset().getName(), buildCost.getAsset().getDescription()), rnd.nextInt(300)+100);
                   costs.add(newBuildCost);
              }
          }
          
          Building returnBuilding= new Building(names.get(rnd.nextInt(names.size())), descriptions.get(rnd.nextInt(descriptions.size())), rnd.nextInt(5)+1);
          returnBuilding.setCost(costs);
          return returnBuilding;
       }
}
