package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class NaturalAssetSQLRepository {
        private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();
        
       public List<NaturalAsset> getnaturalAssets() {
           Query q = em.createQuery("SELECT s FROM NaturalAsset s", NaturalAsset.class);
            return q.getResultList();
    }
    
    public void addNaturalAsset(NaturalAsset pValue)
    {
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
    } 
}
