package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class SpeciesSQLRepository {
    
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public List<Species> getSpecies() {
        return em.createQuery("SELECT s FROM Species s", Species.class).getResultList();
    }
    
    public void addSpecie(Species pValue)
    {
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
    }
    
}
