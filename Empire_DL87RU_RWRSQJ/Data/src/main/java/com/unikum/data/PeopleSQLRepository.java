package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class PeopleSQLRepository {
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();
        
       public List<People> getPeople() {
           Query q = em.createQuery("SELECT p FROM People p", People.class);
            return q.getResultList();
    }
    
    public void addPeople(People pValue)
    {
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
    } 
}
