package com.unikum.data;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
    private String name, password;
    private boolean isAdmin;
    
    @OneToMany(mappedBy = "user")
    private List<Hero> heroes = new ArrayList<>();
    
    
    @OneToMany(mappedBy = "user")
    private List<Empire> empires = new ArrayList<>();
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    public List<Empire> getEmpires() {
        return empires;
    }
    public Empire getEmpireByName(String empireName) {
        Empire e = null;
        for (Empire emp : empires) {
            if (emp.getEmpireName().equals(empireName)) {
                return emp;
            }
        }
        return e;
    }

    public void setEmpires(List<Empire> empires) {
        this.empires = empires;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public User(String name, String password, boolean isAdmin) {
        this.name = name;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public User() {
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
}
