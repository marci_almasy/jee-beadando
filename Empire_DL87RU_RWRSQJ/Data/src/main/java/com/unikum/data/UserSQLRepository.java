package com.unikum.data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UserSQLRepository {
    
    private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public UserSQLRepository() {
    }
    
    
    public List<User> getUsers() {
        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }
    
    //nekünk ez a regisztráció
    public void addUser(User pValue)
    {
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
    }
    
    public User GetByNamePassword(String name, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.select(root);
        cq.where(cb.and(
                cb.equal(root.get("name"), name),               
                cb.equal(root.get("password"), password)));
        return (User)em.createQuery(cq).getSingleResult();
    }
    
    public User GetByName(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.select(root);
        cq.where(cb.equal(root.get("name"), name));            
        return (User)em.createQuery(cq).getSingleResult();
    }
}
