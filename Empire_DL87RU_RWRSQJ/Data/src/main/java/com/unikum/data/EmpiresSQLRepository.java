package com.unikum.data;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class EmpiresSQLRepository {
     private EntityManager em = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public List<Empire> getEmpires() {
        return em.createQuery("SELECT e FROM Empire e", Empire.class).getResultList();
    }

    public EmpiresSQLRepository() {
        
    }
    
  public void addEmpire(Empire empire)
    {
        em.getTransaction().begin();

        for (Stock p : empire.getProduce()) {
            em.persist(p);
        }
        
        for (Stock w : empire.getWareHouse()) {
            em.persist(w);
        }
        
        for (Population p : empire.getPopulation()) {
            em.persist(p);
        }
        /*
        for (Building b : empire.getBuildings()) {
            em.persist(b);
        }*/
        em.persist(empire);
        em.getTransaction().commit();
        em.close();
    }
    
    public List<Empire> GetEmpiresByUser(String empName, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Empire.class);
        Root root = cq.from(Empire.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"), user),cb.equal(root.get("empireName"), empName)));            
        return em.createQuery(cq).getResultList();
    }
    
    public void deleteEmpire(Empire emp){
        em.getTransaction().begin();
        /*
        for (Building building : emp.getBuildings()) {
            building = null;
        }
        for (Population population : emp.getPopulation()) {
            population = null;
        }
        for (Stock stock : emp.getProduce()) {
            stock=null;
        }
        for (Stock stock : emp.getWareHouse()) {
            stock = null;
        }
        emp.getBuildings().clear();
        emp.getPopulation().clear();
        emp.getProduce().clear();
        emp.getWareHouse().clear();
        
        emp.setBuildings(null);
        emp.setPopulation(null);
        emp.setProduce(null);
        emp.setWareHouse(null);
        
        emp.setUser(null);
        */
        em.remove(emp);
        em.getTransaction().commit();
    }
    
    public void editEmpire(Empire empToEdit, String newName, String newDescription){
        em.getTransaction().begin();
        empToEdit.setEmpireName(newName);
        empToEdit.setEmpireDescription(newDescription);
        em.getTransaction().commit();
        em.close();
    }
     public Empire GetEmpireByUser(String empName, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Empire.class);
        Root root = cq.from(Empire.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"), user),cb.equal(root.get("empireName"), empName)));            
        return (Empire)em.createQuery(cq).getResultList().get(0);
    }
}
